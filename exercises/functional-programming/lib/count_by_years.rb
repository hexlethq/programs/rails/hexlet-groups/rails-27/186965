# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users
    .filter { |person| person[:gender] == 'male' }
    .reduce({}) do |hash, person|
      year = person[:birthday].split('-').first
      hash[year] ||= 0
      hash[year] += 1
      hash
    end
end
# END
