# frozen_string_literal: true

# BEGIN
def chars_count_sorted(word)
  word.split('')
      .sort
      .tally
end

def anagramm_filter(word, collection)
  chars_hash = chars_count_sorted(word)

  collection.filter { |word_in_collection| chars_count_sorted(word_in_collection) == chars_hash }
end
# END
