# frozen_string_literal: true

# BEGIN
def get_same_parity(collection)
  return collection if collection.empty?

  parity = collection.first.even? ? :even? : :odd?
  collection.filter(&parity)
end
# END
