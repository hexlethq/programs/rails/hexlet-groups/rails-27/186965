# frozen_string_literal: true

# BEGIN
def reverse(string)
  result = ''

  string.each_char do |c|
    result = c + result
  end

  result
end
# END
