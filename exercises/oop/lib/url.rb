# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

class Url
  include Comparable
  extend Forwardable
  def_delegators :@url, :scheme, :host, :to_s

  def initialize(params)
    @url = URI(params)
  end

  attr_reader :url

  def ==(other)
    url.to_s == other.to_s
  end

  def query_params
    query_to_hash(url)
  end

  def query_param(key, default_value = nil)
    select_param(key, default_value)
  end

  private

  def query_to_hash(url)
    queries = url
              .query
              .split('&')

    queries.each_with_object({}) do |query_pair, hash|
      key, value = query_pair.split('=')
      hash[key.to_sym] = value
    end
  end

  def select_param(param_key, default_value)
    queries = query_to_hash(url)
    queries.default = default_value
    queries[param_key]
  end
end
# END
