# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  def initialize(attrs = {})
    @attributes = {}
    self.class.scheme.each do |name, option|
      value = attrs.key?(name) ? convert_attributes(option, attrs[name]) : nil
      @attributes[name] = value
    end
  end
  
  def attributes
    @attributes
  end

  def convert_attributes(option, value)
    return value if value.nil?

    case option[:type]
    when :integer
      Integer value
    when :string
      String value
    when :datetime
      DateTime.parse(value)
    when :boolean
      !!value 
    end
  end

  module ClassMethods
    attr_reader :scheme

    def attribute(name, option)
      @scheme ||= {}
      @scheme[name] = option

      define_method "#{name}" do
        @attributes[name]
      end

      define_method "#{name}=" do |value|
        scheme = self.class.scheme[name]
        @attributes[name] = convert_attributes(scheme, value)
      end
    end
  end
end
# END
