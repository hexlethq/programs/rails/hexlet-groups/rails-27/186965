# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def test_new_stack_should_be_empty
    stack = Stack.new
    assert stack.empty?
  end

  def test_push_element_in_stack
    stack = Stack.new
    stack.push! 'HTML'
    stack.push! 'CSS'

    assert_equal stack.size, 2
    assert_equal stack.to_a, ['HTML', 'CSS'] 
  end

  def test_pop_element_in_stack
    stack = Stack.new
    stack.push! 'HTML'
    stack.push! 'CSS'
    stack.pop!

    assert_equal stack.size, 1
    assert_equal stack.to_a, ['HTML']
  end

  def test_pop_element_in_empty_stack
    stack = Stack.new
    stack.pop!

    assert stack.empty?
  end

  def test_stack_clear_methods
    stack = Stack.new
    stack.push! 'HTML'
    stack.push! 'CSS'

    assert_equal stack.size, 2

    stack.clear!
    
    assert_equal stack.size, 0
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
