# frozen_string_literal: true

# BEGIN
def split_to_int(string, sep)
  collection = string.split(sep)
  
  collection.collect! { |el| el.to_i }
end

def compare_versions(version_one, version_two)
  major_one, minor_one = split_to_int(version_one, '.')
  major_two, minor_two= split_to_int(version_two, '.')

  if (major_one != major_two)
    return major_one <=> major_two
  end

  minor_one <=> minor_two
end
# END
