# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(text, stop_words)
  # BEGIN
  words = text.split
  censored_text = '$#%!'
  result = []

  words.each do |word|
    result << (stop_words.include?(word) ? censored_text : word)
  end

  result.join(' ')
  # END
end

# rubocop:enable Style/For
