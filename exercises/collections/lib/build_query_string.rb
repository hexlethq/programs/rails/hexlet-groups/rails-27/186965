# frozen_string_literal: true

# rubocop:disable Style/For
# BEGIN
def build_query_string(params)
  sorted_params = params.sort.to_h
  result = []

  sorted_params.each_pair do |param, value|
    result << "#{param}=#{value}"  
  end

  result.join('&')
end
# END
# rubocop:enable Style/For
